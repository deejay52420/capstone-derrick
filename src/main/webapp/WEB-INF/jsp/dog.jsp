<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="/CapstoneDemo/css/bootstrap.css" >
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dawg</title>
</head>
<body>
	<div class="container">
	<div class="col-md-6">
		<form:form commandName="dogForm" role="form">
   <div class="form-group float-label-control">
        <form:label path="name">Name</form:label>
        <form:input class="form-control" path="name" />
 	</div>
     <div class="form-group float-label-control">
        <form:label path="breed">Breed</form:label>
       <form:input class="form-control" path="breed" />
   </div>
   <div class="form-group float-label-control">
        <form:label path="age">Age</form:label>
        <form:input class="form-control" path="age" />
    </div>
    <div class="form-group float-label-control">
        
            <input type="submit" class="btn btn-default" value="Submit"/>
</div> 
</form:form>
		</div>
		</div>

</body>
</html>