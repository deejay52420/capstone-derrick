package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Dog;

@Controller
public class DogController {
	
	@RequestMapping(value="/dog", method=RequestMethod.GET)
    public String greetingForm(Model model) {
        model.addAttribute("dogForm", new Dog());
        return "dog";
    }
	
	 @RequestMapping(value="/dog", method=RequestMethod.POST)
	    public String greetingSubmit(@ModelAttribute Dog dog, Model model) {
	    	//System.out.println(greeting.getContent());
	        model.addAttribute("dog", dog);
	        
	        return "theDog";
	    }
		
}
