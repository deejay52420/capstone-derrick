package com.metlife.dpa.service;

import java.util.List;

import com.metlife.dpa.model.Product;

public interface IProductService {
	List<Product> getProducts();
}
